-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

DROP DATABASE IF EXISTS `belcoders-links`;
CREATE DATABASE `belcoders-links` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `belcoders-links`;

DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL,
  `link` varchar(255) NOT NULL,
  `redirect_numbers` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2019-08-27 13:35:22