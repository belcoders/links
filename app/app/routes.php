<?php
defined("APP_NAME") or die("Hacking attempt!");

$router->add("~^\/()$~", "IndexController@main");
$router->add("~^\/(create)\/$~", "LinksController@create");
$router->add("~^\/(all)\/$~", "LinksController@all");
$router->add("~^\/(?P<code>[a-z0-9]+)\/$~", "LinksController@get");