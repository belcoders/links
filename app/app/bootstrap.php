<?php
defined("APP_NAME") or die("Hacking attempt!");

require(ROOT."/app/config.php");
require(ROOT."/app/libs/core/autoload.php");
require(ROOT."/app/libs/core/functions.php");

$router = new Router();
require(ROOT."/app/routes.php");

if($router->checkUrl(CURRENT_ROUTE)){
	$router->loadController();
}
else send_404();