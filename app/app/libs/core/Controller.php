<?php
defined("APP_NAME") or die("Hacking attempt!");

class Controller {
    public function __construct(){
        R::setup(
            "mysql:host=db;dbname=belcoders-links",
            "root",
            $_ENV['MYSQL_ROOT_PASSWORD']
        );
    }
}