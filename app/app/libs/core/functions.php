<?php
defined("APP_NAME") or die("Hacking attempt!");

function debug($var){
	echo "<pre>".print_r($var)."</pre>";
}

function GEN_TIME($symbols = 4){
	$start_load_time = $_SERVER["REQUEST_TIME_FLOAT"];
    $load_time = microtime(1)-$_SERVER["REQUEST_TIME_FLOAT"];
    return round($load_time, $symbols);
}

function generate_code($count = 6){
	$string = '';
	$symbols = "qwertyuiopasdfghjklzxcvbnm123456789";
	for($i = 0; $i < $count; $i++){
        $string .= substr($symbols, mt_rand(0, strlen($symbols)-1), 1);
    }

    return $string;
}

function send_404(){
	echo "404";
	die();
}

function isLinkAvailible($link)
{
   $curlInit = curl_init($link);
   curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
   curl_setopt($curlInit, CURLOPT_HEADER, true);
   curl_setopt($curlInit, CURLOPT_NOBODY, true);
   curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
   $response = curl_exec($curlInit);
   curl_close($curlInit);
   if($response) return true;
   return false;
}