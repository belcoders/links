<?php
defined("APP_NAME") or die("Hacking attempt!");

class Router
{
	private $routes;
	private $variables;

	private $current;
	private $controller;
	private $action;

	public function __construct(){
		$this->current = CURRENT_ROUTE;
		$this->routes = [];
	}

	public function add($pattern, $route){
		$this->routes[$pattern] = $route;
	}

	public function checkUrl($url){
		foreach($this->routes as $pattern => $route){
			if(preg_match($pattern, CURRENT_ROUTE, $result)){
				$this->current = $route;

				$variables = [];
				foreach($result as $key => $variable){
					if(!is_int($key)) $variables[$key] = $variable;

				}
				$this->variables = $variables;

				$this->controller = explode("@", $this->current)[0];
				$this->action = explode("@", $this->current)[1];

				define("CONTROLLER", $this->controller);
				define("ACTION", $this->action);

				return true;
			}
		}
		return false;
	}

	public function loadController(){
		$controller = $this->controller;
		$action = $this->action."Action";
		$variables = $this->variables;

		$cObject = new $controller();
		$cObject->$action($variables);
	}

	public function getAll(){
		return $this->routes;
	}
}