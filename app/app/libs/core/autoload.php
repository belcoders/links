<?php
defined("APP_NAME") or die("Hacking attempt!");

spl_autoload_register(function($class_name){
    $lib_list = [
    	// Controllers
    	"IndexController" => "/controllers/Index.php",
        "LinksController" => "/controllers/Links.php",
        // Libs
        "R" =>"/libs/Database/rb-mysql.php",
        "Router" =>"/libs/core/router.php",
        "View" =>"/libs/Template/View.php",
        "Controller" => "/libs/core/Controller.php",
    ];
    if(isset($lib_list[$class_name])) require(ROOT."/app".$lib_list[$class_name]);
});