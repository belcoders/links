<?php
defined("APP_NAME") or die("Hacking attempt!");

class View
{
	public static function header($variables = []){
		foreach($variables as $key => $variable){
			$$key = $variable;
		}
		include(THEME_ROOT."/views/template/header.phtml");
	}

	public static function render($view, $variables = []){
		foreach($variables as $key => $variable){
			$$key = $variable;
		}
		include(THEME_ROOT."/views/".$view.".phtml");
	}

	public static function footer($variables = []){
		foreach($variables as $key => $variable){
			$$key = $variable;
		}
		include(THEME_ROOT."/views/template/footer.phtml");
	}
}
