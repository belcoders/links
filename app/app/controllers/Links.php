<?php
defined("APP_NAME") or die("Hacking attempt!");

class LinksController extends Controller
{
	public function getAction($vars){
		$code = strval($vars["code"]);

		$link = R::findOne("links", "code = ?", [$code]);
		if(!isset($link->id)) send_404();

		$link->redirect_numbers++;
		R::store($link);
		die(header("Location: ".$link->link));
	}

	public function createAction(){
		if(METHOD == "POST"){
			$link = $_POST["link"];
			if(
				// Тут, короче, проверки
				!preg_match("~^http(s)://.*\..*$~", $link) && 
				!preg_match("~^http(s)://.*\..*\/.*$~", $link) || 
				strlen($link) > 255
			){
				View::header(["title" => "Создать ссылку"]);
				View::render("messages/alert", [
					"type" => "danger",
					"header" => "Ошибка!",
					"message" => "Используйте правильный шаблон, который указан на странице создания ссылки.",
				]);
				View::footer();
				die();
			}

			// Проверка на доступность ссылки
			if(!isLinkAvailible($link)){
				View::header(["title" => "Создать ссылку"]);
				View::render("messages/alert", [
					"type" => "danger",
					"header" => "Ошибка!",
					"message" => "Проверьте доступность введенной ссылки.",
				]);
				View::footer();
				die();
			}

			$code = generate_code();
			$db_link = R::findOne("links", "link = ?", [$link]);
			if(isset($db_link->id)){
				View::header(["title" => "Создать ссылку"]);
				View::render("messages/alert", [
					"type" => "warning",
					"header" => "Оповещение!",
					"message" => 'Данная ссылка уже была минифицирована.<br>
					Минифицированная ссылка: <b>
						<a href="/'.$db_link->code.'/">http://links.belcoders.ru/'.$db_link->code.'</a>
					</b>.',
				]);
				View::footer();
				die();
			}

			$db_link = R::dispense("links");
			$db_link->code = $code;
			$db_link->link = $link;
			$id = R::store($db_link);
			if(isset($id)){
				View::header(["title" => "Создать ссылку"]);
				View::render("messages/alert", [
					"type" => "success",
					"header" => "Успех!",
					"message" => 'Ваша ссылка успешно минифицирована.<br>
					Минифицированная ссылка: <b>
						<a href="/'.$db_link->code.'/">http://links.belcoders.ru/'.$db_link->code.'</a>
					</b>.',
				]);
				View::footer();
				die();
			}
			else
			{
				View::header(["title" => "Создать ссылку"]);
				View::render("messages/alert", [
					"type" => "danger",
					"header" => "Ошибка!",
					"message" => "Что-то пошло не так...",
				]);
				View::footer();
				die();
			}
		}
		else
		{
			View::header(["title" => "Создать ссылку"]);
			View::render("links/create");
			View::footer();
		}
	}

	public function allAction(){
		$links = R::findAll("links", "ORDER BY id DESC");

		View::header(["title" => "Список ссылок"]);
		View::render("links/all", ["links" => $links]);
		View::footer();
	}
}