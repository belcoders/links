<?php
defined("APP_NAME") or die("Hacking attempt!");

class IndexController extends Controller
{
	public function mainAction($variables){
		View::header([]);
		View::render("index");
		View::footer();
	}
}