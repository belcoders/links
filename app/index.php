<?php
session_start();

define("APP_NAME", "Belcoders Links");
define("APP_VERSION", "0.3");
define("THEME", "default");
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
define("CURRENT_ROUTE", $_SERVER["REQUEST_URI"]);
define("METHOD", $_SERVER["REQUEST_METHOD"]);
define("THEME_ROOT", ROOT."/themes/".THEME);

require(ROOT."/app/bootstrap.php");