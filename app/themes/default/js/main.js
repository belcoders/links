function portfolio_list(){
    if($("#portfolio-list").html()){
        $("#portfolio-list").html('<div class="alert alert-info">Загрузка данных...</div>');

        $.get("/api/?mode=portfolio", {}, function(data){
            data = JSON.parse(data);
            $("#portfolio-list").html('');
            data.forEach(function(item, i, portfolio){
                $("#portfolio-list").append('<div class="col-lg-4">');
                $("#portfolio-list").append('<h2 align="center">'+data[i]["title"]+'</h2>');
            });
        });
    }
}

$(document).ready(function(){
    $("#login-btn").bind("click", function(){
        $("#login-btn").val("Вход...").addClass("disabled");
        $.get("/api/?mode=auth", {login: $("#login").val(), password: $("#password").val()}, function(data){
            if(data == "success"){
                $("#messages").removeClass().addClass("alert").addClass("alert-success").text("Вход успешно выполнен. Перенаправление на главную страницу...");
                setTimeout(function(){
                    window.location.href = "/";
                }, 2000)
            }
            else
            {
                $("#messages").addClass("alert").addClass("alert-danger").text(data);
                $("#login-btn").val("Войти").removeClass("disabled");
            }
        });
    });
});